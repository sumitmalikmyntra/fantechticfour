package com.xavidop.alexa.servlet;

import com.amazon.ask.Skill;
import com.amazon.ask.Skills;
import com.amazon.ask.servlet.SkillServlet;
import com.xavidop.alexa.handlers.*;
import com.xavidop.alexa.interceptors.request.LocalizationRequestInterceptor;
import com.xavidop.alexa.interceptors.request.LogRequestInterceptor;
import com.xavidop.alexa.interceptors.response.LogResponseInterceptor;

public class AlexaServlet extends SkillServlet {

    public AlexaServlet() {
        super(getSkill());
    }

    private static Skill getSkill() {
        return Skills.standard()
                .addRequestHandlers(
                        new CancelandStopIntentHandler(),
                        new CouponIntentHandler(),
                        new HelpIntentHandler(),
                        new LaunchRequestHandler(),
                        new CartIntentHandler(),
                        new HindiCouponIntentHandler(),
                        new RecommendIntentHandler(),
                        new HindiRecommendIntentHandler(),
                        new HindiCartIntentHandler(),
                        new SessionEndedRequestHandler(),
                        new FallbackIntentHandler(),
                        new ErrorHandler())
                .addExceptionHandler(new MyExceptionHandler())
                .addRequestInterceptors(
                        new LogRequestInterceptor(),
                        new LocalizationRequestInterceptor())
                .addResponseInterceptors(new LogResponseInterceptor())
                // Add your skill id below
                //.withSkillId("[unique-value-here]")
                .build();
    }

}