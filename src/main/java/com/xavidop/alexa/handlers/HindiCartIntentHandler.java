package com.xavidop.alexa.handlers;

import cartclient.*;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.Slot;
import com.amazon.ask.model.ui.Image;
import lombok.NonNull;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static com.amazon.ask.request.Predicates.intentName;

@Service
public class HindiCartIntentHandler implements RequestHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(CartIntentHandler.class);

    private RestTemplate restTemplate = new RestTemplate();

    private static final String STYLE_CART_KEY = "styleid";

    //public CartIntentHandler(@NonNull RestTemplate restTemplate)
   /* {
        this.restTemplate = restTemplate;
    }*/

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("HindiCartIntent"));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {

        IntentRequest intentRequest = (IntentRequest) input.getRequestEnvelope().getRequest();
        Map<String, Slot> slots = intentRequest.getIntent().getSlots();
        String speechText = "";
        CartClient cartClient = new CartClient(restTemplate);
        CartGetClient cartGetClient = new CartGetClient(restTemplate);
        StyleInfoClient styleInfoClient = new StyleInfoClient(restTemplate);
        CartGetImageClient cartGetImageClient = new CartGetImageClient(restTemplate);
        List<String> cartImages = new ArrayList<>();
        List<String> finalImages = new ArrayList<>();
        try {

            if(MapUtils.isNotEmpty(slots) && slots.containsKey(STYLE_CART_KEY)) {
                String styleId = slots.get(STYLE_CART_KEY).getValue();
                Long skuId = 0L;

                List<StyleInventory> styleInventoryList = styleInfoClient.fetchStyleData(styleId);
                ListIterator<StyleInventory> styleInventoryListIterator = styleInventoryList.listIterator();
                boolean inventoryFlag = false;
                while (styleInventoryListIterator.hasNext()) {
                    StyleInventory styleInventory = styleInventoryListIterator.next();
                    if (styleInventory.isAvailable()) {
                        skuId = styleInventory.getSkuid();
                        inventoryFlag = true;
                        break;
                    }
                }

                List<CartProducts> cartProductsList = cartClient.fetchCartData(styleId, skuId);
                CartCharges cartCharges = cartGetClient.fetchCartData();
                ListIterator<CartProducts> cartProductsIterator = cartProductsList.listIterator();
                if (cartProductsList == null) {
                    speechText = "अरे! आपके कार्ट आइटम लाते समय हमें आपकी ओर से थोड़ी परेशानी हो रही है। कुछ देर बाद कोशिश करें।";
                } else {
                    StringBuilder speechBuilder = new StringBuilder("");
                    if(inventoryFlag== true)
                        speechBuilder.append("अरे हमने आपका उत्पाद जोड़ दिया और आपकी गाड़ी है");
                    else if(inventoryFlag == false)
                        speechBuilder.append("\n" +
                                "अरे यह आपके उत्पाद को जोड़ नहीं सकता क्योंकि यह स्टॉक से बाहर है लेकिन हमने आपकी गाड़ी ले ली है");
                    while (cartProductsIterator.hasNext()) {
                        speechBuilder.append(cartProductsIterator.next().getTitle() + "<break time=\"1s\"/>");
                    }
                    speechBuilder.append("शिपिंग शुल्क हैं:" + cartCharges.getShippingApplicableCharge() + "<break time=\"1s\"/>");
                    cartImages = cartGetImageClient.fetchCartData();
                    if(cartImages!=null)
                    {
                        ListIterator<String> cartImagesIterator = cartImages.listIterator();

                        while(cartImagesIterator.hasNext())
                        {
                            String image = cartImagesIterator.next();
                            image = image.replaceAll("http", "https");
                            finalImages.add(image);
                        }
                    }
                    speechText = speechBuilder.toString();
                }

            }
        }
        catch (Exception e){
            LOGGER.error("Error while fetching data from Cart");
        }

        //speechText = LocalizationManager.getInstance().getMessage("BEST_COUPON");
        return input.getResponseBuilder()
                .withSpeech(speechText)
                .withSimpleCard("कार्ट आइटम", speechText)
                .withStandardCard("जोड़े गए आइटम", speechText, Image.builder().withLargeImageUrl(finalImages.get(0)).build())
                .withReprompt(speechText)
                .build();
    }

}
