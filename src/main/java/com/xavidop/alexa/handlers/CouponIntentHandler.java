package com.xavidop.alexa.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.Slot;
import com.amazon.ask.model.ui.Image;
import com.myntra.coupon.response.MyntCouponPdpResponse;
import com.myntra.coupon.response.entry.MyntCouponPdpEntry;
import com.xavidop.alexa.localization.LocalizationManager;
import com.xavidop.alexa.service.CouponService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import java.util.Map;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

public class CouponIntentHandler implements RequestHandler {

    private static final String STYLE_SLOT_KEY = "styleid";

    CouponService couponService = new CouponService();

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("CouponIntent"));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        IntentRequest intentRequest = (IntentRequest) input.getRequestEnvelope().getRequest();
        Map<String, Slot> slots = intentRequest.getIntent().getSlots();
        String couponCode = "";
        String couponDiscount = "";
        String speechText = "";
        if (MapUtils.isNotEmpty(slots) && slots.containsKey(STYLE_SLOT_KEY)) {
            String styleId = slots.get(STYLE_SLOT_KEY).getValue();
            MyntCouponPdpResponse pdpResponse = couponService.getCouponForStyle(Long.parseLong(styleId));
            if(CollectionUtils.isNotEmpty(pdpResponse.getData())){
                MyntCouponPdpEntry bestCoupon = pdpResponse.getData().get(0);
                couponCode = bestCoupon.getCoupon();
                couponDiscount = bestCoupon.getCoupondiscount();
                speechText = LocalizationManager.getInstance().getMessage("BEST_COUPON") + couponCode + ". Get extra " + couponDiscount;
            } else{
                speechText = "This product is already at its best price.";
            }
        }

        return input.getResponseBuilder()
                .withSpeech(speechText)
                .withSimpleCard("Best Offer", speechText)
                .withReprompt(speechText)
                .build();
    }

}