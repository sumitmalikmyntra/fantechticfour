package com.xavidop.alexa.handlers;

import cartclient.CartClient;
import cartclient.CartProducts;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.Slot;
import com.amazon.ask.model.ui.Image;
import com.xavidop.alexa.domain.SearchProduct;
import com.xavidop.alexa.domain.SearchProducts;
import com.xavidop.alexa.domain.SearchResponse;
import com.xavidop.alexa.service.SearchClient;
import lombok.NonNull;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static com.amazon.ask.request.Predicates.intentName;

@Service
public class RecommendIntentHandler implements RequestHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(RecommendIntentHandler.class);


    private static final String BRAND_SLOT_KEY = "brand";
    private static final String QUERY_SLOT_KEY = "query";

    //public CartIntentHandler(@NonNull RestTemplate restTemplate)
   /* {
        this.restTemplate = restTemplate;
    }*/

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("RecommendIntent"));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {

        IntentRequest intentRequest = (IntentRequest) input.getRequestEnvelope().getRequest();
        Map<String, Slot> slots = intentRequest.getIntent().getSlots();
        String speechText = "";
        String image = null;
        SearchClient client = new SearchClient();
        try {

            if(MapUtils.isNotEmpty(slots)) {
                String brand = null;
                String query = null;
                if (slots.containsKey(BRAND_SLOT_KEY))
                        brand = slots.get(BRAND_SLOT_KEY).getValue();
                if (slots.containsKey(QUERY_SLOT_KEY))
                    query = slots.get(QUERY_SLOT_KEY).getValue();
                SearchProducts products = client.recommendStyles(query, capitalizeFirstLetter(brand), null);
                if (products.getProducts() == null || products.getProducts().size() == 0) {
                    speechText = "Hey! We are having a bit of trouble on our side. Try" +
                            " after sometime.";
                } else {
                    StringBuilder speechBuilder = new StringBuilder("We have " + products.getProducts().size() + " " +
                            "items for you <break time=\"1s\"/> ");
                    Iterator<SearchProduct> productsIterator = products.getProducts().iterator();
                    while (productsIterator.hasNext()) {
                        SearchProduct product = productsIterator.next();
                        image = product.getSearchImage();
                        image = image.replaceAll("http", "https");
                        speechBuilder.append(product.getProductName() + " is available at rupees " + product.getMrp()
                                + "<break time=\"1s\"/> ");
                    }
                    speechText = speechBuilder.toString();
                }
            }
        }
        catch (Exception e){
            LOGGER.error("Error while fetching data from Cart");
        }

        //speechText = LocalizationManager.getInstance().getMessage("BEST_COUPON");
        return input.getResponseBuilder()
                .withSpeech(speechText)
                .withStandardCard("Recommended items", speechText, Image.builder().withLargeImageUrl(image).build())
                .withReprompt(speechText)
                .build();
    }

    private String capitalizeFirstLetter(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

}
