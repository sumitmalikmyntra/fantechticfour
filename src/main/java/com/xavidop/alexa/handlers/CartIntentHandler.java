package com.xavidop.alexa.handlers;

import cartclient.*;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.Slot;
import com.amazon.ask.model.interfaces.display.*;
import com.amazon.ask.model.ui.Image;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static com.amazon.ask.request.Predicates.intentName;

@Service
public class CartIntentHandler implements RequestHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(CartIntentHandler.class);

    private RestTemplate restTemplate = new RestTemplate();

    private static final String STYLE_CART_KEY = "stylecode";

    //public CartIntentHandler(@NonNull RestTemplate restTemplate)
   /* {
        this.restTemplate = restTemplate;
    }*/

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("CartIntent"));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {

        String title = "Your Cart";
        String primaryText = "";
        String secondaryText = "";
        //String speechText = "Sample Output Speech Text";
        String imageUrl = "imageUrl";

        IntentRequest intentRequest = (IntentRequest) input.getRequestEnvelope().getRequest();
        Map<String, Slot> slots = intentRequest.getIntent().getSlots();
        String speechText = "";
        CartClient cartClient = new CartClient(restTemplate);
        CartGetClient cartGetClient = new CartGetClient(restTemplate);
        StyleInfoClient styleInfoClient = new StyleInfoClient(restTemplate);
        CartGetImageClient cartGetImageClient = new CartGetImageClient(restTemplate);
        List<String> cartImages = new ArrayList<>();
        List<String> finalImages = new ArrayList<>();
        try {

            if(MapUtils.isNotEmpty(slots) && slots.containsKey(STYLE_CART_KEY)) {
                String styleId = slots.get(STYLE_CART_KEY).getValue();
                Long skuId = 0L;

                List<StyleInventory> styleInventoryList = styleInfoClient.fetchStyleData(styleId);
                ListIterator<StyleInventory> styleInventoryListIterator = styleInventoryList.listIterator();
                boolean inventoryFlag = false;
                while (styleInventoryListIterator.hasNext()) {
                    StyleInventory styleInventory = styleInventoryListIterator.next();
                    if (styleInventory.isAvailable()) {
                        skuId = styleInventory.getSkuid();
                        inventoryFlag = true;
                        break;
                    }
                }

                List<CartProducts> cartProductsList = cartClient.fetchCartData(styleId, skuId);
                CartCharges cartCharges = cartGetClient.fetchCartData();
                ListIterator<CartProducts> cartProductsIterator = cartProductsList.listIterator();
                if (cartProductsList == null) {
                    speechText = "Hey! We are having a bit of trouble on your side while fetching your Cart Items. Try after sometime.";
                } else {
                    StringBuilder speechBuilder = new StringBuilder("");
                    if(inventoryFlag== true)
                        speechBuilder.append("Hey we added your product and your cart is");
                    else if(inventoryFlag == false)
                        speechBuilder.append("Hey couldn't add your product since it is out of stock but we fetched your cart");
                    while (cartProductsIterator.hasNext()) {
                        speechBuilder.append(cartProductsIterator.next().getTitle() + "<break time=\"1s\"/>");
                    }
                    speechBuilder.append("ShippingChargesAre:" + cartCharges.getShippingApplicableCharge() + "<break time=\"1s\"/>");
                    cartImages = cartGetImageClient.fetchCartData();
                    if(cartImages!=null)
                    {
                        ListIterator<String> cartImagesIterator = cartImages.listIterator();

                        while(cartImagesIterator.hasNext())
                        {
                            String image = cartImagesIterator.next();
                            image = image.replaceAll("http", "https");
                            finalImages.add(image);
                        }
                    }
                    speechText = speechBuilder.toString();
                }

            }
        }
        catch (Exception e){
            LOGGER.error("Error while fetching data from Cart");
        }
        com.amazon.ask.model.interfaces.display.Image image = getImage(finalImages.get(0),finalImages.get(1));
        //com.amazon.ask.model.interfaces.display.Image image1 = getImage(finalImages.get(0));

        Template template = getBodyTemplate3(title, primaryText, secondaryText, image);
        //Template template2 = getBodyTemplate3(title, primaryText, secondaryText, image1);
        //speechText = LocalizationManager.getInstance().getMessage("BEST_COUPON");
        return input.getResponseBuilder()
                .withSpeech(speechText)
                .withSimpleCard("CartItems", speechText)
                //.withStandardCard("Added items", speechText, Image.builder().withLargeImageUrl(finalImages.get(0)).build())
                .addRenderTemplateDirective(template)
                //.addRenderTemplateDirective(template2)
                .withReprompt(speechText)
                .build();
    }

    private com.amazon.ask.model.interfaces.display.Image getImage(String imageUrl1, String imageUrl2) {
        List<ImageInstance> instances = new ArrayList<>();
        ImageInstance imageInstance1 = getImageInstanc(imageUrl1);
        ImageInstance imageInstance2 = getImageInstanc(imageUrl2);
        instances.add(imageInstance1);
        instances.add(imageInstance2);
        return com.amazon.ask.model.interfaces.display.Image.builder()
                .withSources(instances)
                .build();

    }

   /* private com.amazon.ask.model.interfaces.display.Image getImage(String imageUrl) {
        List<ImageInstance> instances = getImageInstance(imageUrl);
        return com.amazon.ask.model.interfaces.display.Image.builder()
                .withSources(instances)
                .build();

    }*/

    private ImageInstance getImageInstanc(String imageUrl) {

        ImageInstance instance = ImageInstance.builder()
                .withUrl(imageUrl)
                .build();

        return instance;
    }

    private List<ImageInstance> getImageInstance(String imageUrl) {
        List<ImageInstance> instances = new ArrayList<>();
        ImageInstance instance = ImageInstance.builder()
                .withUrl(imageUrl)
                .build();
        instances.add(instance);
        return instances;
    }

    private Template getBodyTemplate3(String title, String primaryText, String secondaryText, com.amazon.ask.model.interfaces.display.Image image) {
        return BodyTemplate3.builder()
                .withImage(image)
                .withTitle(title)
                .withTextContent(getTextContent(primaryText, secondaryText))
                .build();
    }

    private TextContent getTextContent(String primaryText, String secondaryText) {
        return TextContent.builder()
                .withPrimaryText(makeRichText(primaryText))
                .withSecondaryText(makeRichText(secondaryText))
                .build();
    }

    private RichText makeRichText(String text) {
        return RichText.builder()
                .withText(text)
                .build();
    }

}
