package com.xavidop.alexa.domain;

import java.util.List;

/**
 * Created by 300068597 on 16/07/20.
 */
public class SearchResponse {
   SearchProducts response;

   public SearchProducts getResponse() {
       return response;
   }

   public void setResponse(SearchProducts response) {
       this.response = response;
   }
}
