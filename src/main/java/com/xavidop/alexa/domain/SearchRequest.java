package com.xavidop.alexa.domain;

import com.amazonaws.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 300068597 on 16/07/20.
 */
public class SearchRequest {
    String query;
    boolean returnDocs = true;
    boolean isFacet = false;
    int start = 0;
    int rows = 5;
    AppliedParams appliedParams;

    public String getQuery() {
        return query;
    }

    public boolean getReturnDocs() {
        return returnDocs;
    }

    public boolean getIsFacet() {
        return isFacet;
    }

    public int getStart() {
        return start;
    }

    public int getRows() {
        return rows;
    }

    public AppliedParams getAppliedParams() {
        return appliedParams;
    }

    public SearchRequest(String query, String brand, String gender) {
        this.query = query;
        this.appliedParams = new AppliedParams(brand, gender);
    }
}

class AppliedParams {
    List<FilterValue> filters;

    public  List<FilterValue> getFilters() {
        return filters;
    }

    public AppliedParams(String brand, String gender) {
        filters = new ArrayList<>();
        if (StringUtils.hasValue(brand)) {
            filters.add(new FilterValue("brand", brand));
        }
        if (StringUtils.hasValue(gender)) {
            filters.add(new FilterValue("gender", gender));
        }
    }
}

class FilterValue {
    String id;
    List<String> values;

    public String getId() {
        return id;
    }

    public List<String> getValues() {
        return values;
    }

    public FilterValue(String name, String value) {
        this.id = name;
        this.values = new ArrayList<>();
        values.add(value);
    }
}
