package com.xavidop.alexa.domain;

import java.util.List;

/**
 * Created by 300068597 on 16/07/20.
 */
public class SearchProducts {
    List<SearchProduct> products;

    public List<SearchProduct> getProducts() {
        return  products;
    }

    public void setProducts(List<SearchProduct> products) {
        this.products = products;
    }
}
