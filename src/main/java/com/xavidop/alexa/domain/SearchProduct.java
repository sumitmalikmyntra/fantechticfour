package com.xavidop.alexa.domain;

import lombok.Data;

/**
 * Created by 300068597 on 16/07/20.
 */
@Data
public class SearchProduct {
    String productName;
    double mrp;
    double price;
    String searchImage;

}
