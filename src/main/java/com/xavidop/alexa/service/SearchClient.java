package com.xavidop.alexa.service;

import com.myntra.coupon.response.MyntCouponPdpResponse;
import java.util.Arrays;

import com.xavidop.alexa.domain.SearchProducts;
import com.xavidop.alexa.domain.SearchRequest;
import com.xavidop.alexa.domain.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SearchClient {


    private RestTemplate restTemplate = new RestTemplate();

    final String search_url = "http://search.stage.myntra.com/search-service/searchservice/search/v2/getsearchresults/";

    public SearchProducts recommendStyles(String query, String brand, String gender){
        HttpEntity<SearchRequest> request = new HttpEntity<SearchRequest>(new SearchRequest(query, brand,
                gender), getHeadersEntity());
        ResponseEntity<SearchResponse> responseEntity = restTemplate.exchange(search_url, HttpMethod.POST,
                request, SearchResponse.class);
        return responseEntity.getBody().getResponse();
    }


    private HttpHeaders getHeadersEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("x-mynt-ctx","storeid=2297");
        return headers;
    }

}
