package com.xavidop.alexa.service;

import com.myntra.coupon.response.MyntCouponPdpResponse;
import java.util.Arrays;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class CouponService {

  private RestTemplate restTemplate = new RestTemplate();

  final String coupon_pdp_url = "http://redispub-coupon.dockins.myntra.com/coupons/pdp/";

  public MyntCouponPdpResponse getCouponForStyle(Long styleId){
    HttpEntity<String> headersEntity = getHeadersEntity();
    ResponseEntity<MyntCouponPdpResponse> responseEntity = restTemplate.exchange(coupon_pdp_url+styleId, HttpMethod.GET, headersEntity, MyntCouponPdpResponse.class);
    return responseEntity.getBody();
  }

  public MyntCouponPdpResponse getCouponForStyleSeller(Long styleId, Long sellerId){
    HttpEntity<String> headersEntity = getHeadersEntity();
    ResponseEntity<MyntCouponPdpResponse> responseEntity = restTemplate.exchange(coupon_pdp_url+styleId+"/"+sellerId, HttpMethod.GET, headersEntity, MyntCouponPdpResponse.class);
    return responseEntity.getBody();
  }

  private HttpEntity<String> getHeadersEntity() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    headers.set("x-mynt-ctx","storeid=2297;uidx=10068e8b.b047.4d7f.8420.585fd1d14a2b6dEqV8GwYv;");
    return new HttpEntity<>("parameters", headers);
  }

}
