package cartclient;


import com.myntra.monk.core.XMetaRequestContext;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Service
public class StyleInfoClient {

    private final static Logger LOGGER = LoggerFactory.getLogger(StyleInfoClient.class);

    private RestTemplate restTemplate;




    @Autowired
    public StyleInfoClient(@NonNull RestTemplate restTemplate){

        this.restTemplate = restTemplate;
    }

    public List<StyleInventory> fetchStyleData(String styleId) throws Exception{

        ResponseEntity<String> response;
        ResponseEntity<StyleProductEntry> styleProductEntryResponseEntity;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
        headers.add(HttpHeaders.ACCEPT, "application/json");
        headers.add("x-mynt-ctx","storeid=2297");
        headers.add("m-storeid","2297");
        // headers.add(XMetaRequestContext.X_META_CTX_STORE_ID, tenantId);

        StyleSearchBody styleSearchBody = new StyleSearchBody();


        List<Long> styleIds = new ArrayList<>();
        styleIds.add(Long.parseLong(styleId));

        styleSearchBody.setStyleIds(styleIds);
        styleSearchBody.setOnlyActiveStyles(true);
        styleSearchBody.setOutOfStock(false);
        styleSearchBody.setFilter(false);

        DefaultUriBuilderFactory factory = new DefaultUriBuilderFactory();
        URI uri = factory.uriString(buildQueryParams("2297")).build();

        HttpEntity<StyleSearchBody> httpEntity = new HttpEntity(styleSearchBody,headers);

        response = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, String.class);
        styleProductEntryResponseEntity = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, StyleProductEntry.class);


        System.out.println(response);

        System.out.println(styleProductEntryResponseEntity.getBody().getData().getProducts().get(0).getInventoryInfo());

        if (styleProductEntryResponseEntity.getStatusCode() == HttpStatus.OK && styleProductEntryResponseEntity.hasBody() && styleProductEntryResponseEntity.getBody() != null) {
            List<StyleInventory> styleProductsList = styleProductEntryResponseEntity.getBody().getData().getProducts().get(0).getInventoryInfo();
            return styleProductsList;

        }
        return null;

    }

    private String buildQueryParams(final String tenantId) {

        final StringBuilder stringBuilder = new StringBuilder("http://search.stage.myntra.com/search-service/searchservice/search/getStyle");
        return stringBuilder.toString();
    }
}
