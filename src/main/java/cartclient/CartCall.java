package cartclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public class CartCall {


    public static void main(String[] args){

        RestTemplate restTemplate = new RestTemplate();
        CartClient cartClient = new CartClient(restTemplate);
        CartGetClient cartGetClient = new CartGetClient(restTemplate);
        StyleInfoClient styleInfoClient = new StyleInfoClient(restTemplate);
        CartGetImageClient cartGetImageClient = new CartGetImageClient(restTemplate);

        try {
            //cartClient.fetchCartData("1531",3831L);
            //cartGetClient.fetchCartData();
            //styleInfoClient.fetchStyleData("1536");
            cartGetImageClient.fetchCartData();
        }
        catch (Exception e){

        }
    }
}
