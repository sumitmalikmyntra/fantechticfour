package cartclient;

import lombok.Data;

import java.util.List;

@Data
public class CartGetProductEntry {

    private CartCharges shippingData;
    private List<CartGetProducts> products;
}
