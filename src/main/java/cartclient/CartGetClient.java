package cartclient;


import com.myntra.monk.core.XMetaRequestContext;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.net.URI;
import java.util.List;

@Service
public class CartGetClient {

    private final static Logger LOGGER = LoggerFactory.getLogger(CartGetClient.class);

    private RestTemplate restTemplate;

    private static final String STYLE_CART_KEY = "styleid";


    @Autowired
    public CartGetClient(@NonNull RestTemplate restTemplate){

        this.restTemplate = restTemplate;
    }

    public CartCharges fetchCartData() throws Exception{

        ResponseEntity<String> response;
        ResponseEntity<CartGetProductEntry> cartProductEntryResponseEntity;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
        headers.add(HttpHeaders.ACCEPT, "application/json");
        headers.add("x-mynt-ctx","storeid=2297;nidx=abcd.2defgh-11ea-9831-000d3af27f0e;uidx=a5e00228.e00b.44e4.ae83.94503e934fb7L52TTreK3T");
        // headers.add(XMetaRequestContext.X_META_CTX_STORE_ID, tenantId);

        /*CartBody cartBody = new CartBody();

        cartBody.setSkuId(3851L);
        cartBody.setStyleId(Long.parseLong(styleId));
        cartBody.setSellerPartnerId(4024L);
        cartBody.setQuantity(1L);*/

        DefaultUriBuilderFactory factory = new DefaultUriBuilderFactory();
        URI uri = factory.uriString(buildQueryParams("2297")).build();

        HttpEntity httpEntity = new HttpEntity(headers);

        response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, String.class);
        cartProductEntryResponseEntity = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, CartGetProductEntry.class);


        System.out.println(response);

        System.out.println(cartProductEntryResponseEntity.getBody().getShippingData().getShippingApplicableCharge());

        System.out.println(cartProductEntryResponseEntity.getBody().getProducts().get(0).getImage().getImage());

        if (cartProductEntryResponseEntity.getStatusCode() == HttpStatus.OK && cartProductEntryResponseEntity.hasBody() && cartProductEntryResponseEntity.getBody() != null) {
            CartCharges cartCharges = cartProductEntryResponseEntity.getBody().getShippingData();
            return cartCharges;

        }
        return null;

    }

    private String buildQueryParams(final String tenantId) {

        final StringBuilder stringBuilder = new StringBuilder("http://cartservicev2.stage.myntra.com/myntra-absolut-service/cart/v2/default");
        return stringBuilder.toString();
    }
}
