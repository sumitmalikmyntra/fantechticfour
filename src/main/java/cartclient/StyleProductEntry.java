package cartclient;

import lombok.Data;

@Data
public class StyleProductEntry {

    private StyleData data;
}
