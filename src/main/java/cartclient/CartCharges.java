package cartclient;

import lombok.Data;

@Data
public class CartCharges {

    private double shippingApplicableCharge;
}
