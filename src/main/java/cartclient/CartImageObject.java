package cartclient;

import lombok.Data;

@Data
public class CartImageObject {

    private String image;
}
