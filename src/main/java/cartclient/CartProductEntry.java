package cartclient;

import lombok.Data;

import java.util.List;

@Data
public class CartProductEntry {

    private List<CartProducts> products;
}
