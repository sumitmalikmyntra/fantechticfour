package cartclient;

import lombok.Data;

import java.util.List;

@Data
public class StyleProducts {

    private List<StyleInventory> inventoryInfo;
}
