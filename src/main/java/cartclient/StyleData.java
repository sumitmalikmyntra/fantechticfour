package cartclient;

import lombok.Data;

import java.util.List;

//Hindi, Image
@Data
public class StyleData {

    private List<StyleProducts> products;
}
