package cartclient;

import lombok.Data;

@Data
public class CartBody {

    public Long skuId;
    public Long styleId;
    public Long sellerPartnerId;
    public Long quantity;
}
