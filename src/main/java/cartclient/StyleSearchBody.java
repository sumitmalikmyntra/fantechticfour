package cartclient;

import lombok.Data;

import java.util.List;

@Data
public class StyleSearchBody {

    private List<Long> styleIds;
    private boolean onlyActiveStyles;
    private boolean outOfStock;
    private boolean filter;

}
