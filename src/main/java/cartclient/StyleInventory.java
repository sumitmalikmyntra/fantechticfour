package cartclient;

import lombok.Data;

@Data
public class StyleInventory {

    private Long skuid;
    private boolean available;
}
