package cartclient;


import com.myntra.monk.core.XMetaRequestContext;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.net.URI;
import java.util.List;

@Service
public class CartClient {

    private final static Logger LOGGER = LoggerFactory.getLogger(CartClient.class);

    private RestTemplate restTemplate;

    private static final String STYLE_CART_KEY = "styleid";


    @Autowired
    public CartClient(@NonNull RestTemplate restTemplate){

        this.restTemplate = restTemplate;
    }

    public List<CartProducts> fetchCartData(String styleId,Long skuId) throws Exception{

        ResponseEntity<String> response;
        ResponseEntity<CartProductEntry> cartProductEntryResponseEntity;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
        headers.add(HttpHeaders.ACCEPT, "application/json");
        headers.add("x-mynt-ctx","storeid=2297;nidx=abcd.2defgh-11ea-9831-000d3af27f0e;uidx=63b701bc.26e3.4fde.bb4f.63630c3ecee7Tzdiblft5H");
       // headers.add(XMetaRequestContext.X_META_CTX_STORE_ID, tenantId);

        CartBody cartBody = new CartBody();

        cartBody.setSkuId(skuId);
        cartBody.setStyleId(Long.parseLong(styleId));
        cartBody.setSellerPartnerId(4024L);
        cartBody.setQuantity(1L);

        DefaultUriBuilderFactory factory = new DefaultUriBuilderFactory();
        URI uri = factory.uriString(buildQueryParams("2297")).build();

        HttpEntity<CartBody> httpEntity = new HttpEntity(cartBody, headers);

        response = restTemplate.exchange(uri, HttpMethod.PUT, httpEntity, String.class);
        cartProductEntryResponseEntity = restTemplate.exchange(uri, HttpMethod.PUT, httpEntity, CartProductEntry.class);


        System.out.println(response);

        System.out.println(cartProductEntryResponseEntity.getBody().getProducts().get(1).getTitle());

        if (cartProductEntryResponseEntity.getStatusCode() == HttpStatus.OK && cartProductEntryResponseEntity.hasBody() && cartProductEntryResponseEntity.getBody() != null) {
            List<CartProducts> cartProductsList = cartProductEntryResponseEntity.getBody().getProducts();
            return cartProductsList;

        }
        return null;

    }

    private String buildQueryParams(final String tenantId) {

        final StringBuilder stringBuilder = new StringBuilder("http://cartservicev2.stage.myntra.com/myntra-absolut-service/cart/v2/default/add");
        return stringBuilder.toString();
    }
}
