package cartclient;

import lombok.Data;

@Data
public class CartGetProducts {

    private CartImageObject image;
}
